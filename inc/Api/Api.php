<?php 

/**
 * 
 * @package api_onesignal_push_notification
 * 
 */

 
namespace Inc\Api;

class Api 
{
	public function register() {

        add_action( 'rest_api_init', array($this,'api_opn'));
	}

    public function api_opn()
    {
        register_rest_route( 'api_opn/v2', '/send', array(
            'methods' => 'POST',
            'callback' => array($this,'push_notification'),
        ) );
    }

    public function push_notification($data)
    {
        $id = $data["id"];
        $title = base64_decode($data["title"]);

        $app_id = $data["app_id"];
        $_app_id = esc_attr( get_option( 'app_id_api_opn_meksiabdou' ));

        $data = array(
            'app_id' => $_app_id,
            "data" => array("action" => "article" , "id" => $id),
            "contents" => array("en" => $title),
            'included_segments' => array('All'),
            'web_url' => get_site_url('','?p='.$id),
            'isIos' => true,
            'isAnyWeb' => flase,
            'isAndroid' => true
        );

        if(empty($id) || empty($title) || empty($app_id) || empty($_app_id) || $app_id != $_app_id) 
        {
            return array("status" => "Error");
        }else{
            $return = json_decode($this->Curl($data),false);
            return($return);
        }
    }

    private function Curl($query)
    {
        $fields = json_encode($query);
        $_key = esc_attr( get_option( 'Key_api_opn_meksiabdou' ));

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8',
												   'Authorization: Basic '.$_key.''));
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($ch, CURLOPT_HEADER, FALSE);
		curl_setopt($ch, CURLOPT_POST, TRUE);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

		$response = curl_exec($ch);
        curl_close($ch);

		return $response;
    }
}