<?php
/**
 * @package  api_onesignal_push_notification
 */
namespace Inc\Base;

class Deactivate
{
	public static function deactivate() {
		flush_rewrite_rules();
	}
}