<?php 

/**
 * 
 * @package api_onesignal_push_notification
 * 
 */

 
namespace Inc\Pages;

use \Inc\Base\BaseController;


class Admin extends BaseController
{
	public function register() {
		add_action( 'admin_menu', array( $this, 'add_admin_page' ) );
		add_action( 'admin_menu', array( $this, 'add_admin_sub_page' ) );
		add_action( 'admin_init', array( $this, 'registerFields' ) );
	}

	public function add_admin_page() 
	{
		add_menu_page( 'Api (Onesignal PN)', 
		'Onesignal (Onesignal PN)', 
		'manage_options', 
		'api_onesignal_push_notification_meksiabdou', 
		function () { require_once $this->plugin_path . 'templates/admin.php'; }, 
		'dashicons-share-alt2', 110 );
	}

	public function add_admin_sub_page(){

		add_submenu_page("api_onesignal_push_notification_meksiabdou",
		'Key Api Onesignal', 
		'Settings', 
		'manage_options', 
		'api_opn_settings_meksiabdou', 
		function () { require_once $this->plugin_path . 'templates/settings.php'; } );
	}

	public function setFields()
	{		
		add_settings_field(
			'Key_api_opn_meksiabdou',
			'YOUR REST API KEY',
			array($this,"Key_api_opn_meksiabdou"),
			'api_opn_settings_meksiabdou',
			'api_opn_admin_index_meksaibdou',
			array('label_for' => 'Key_api_opn_meksiabdou')
		);
		add_settings_field(
			'app_id_api_opn_meksiabdou',
			'APP ID',
			array($this,"app_id_api_opn_meksiabdou"),
			'api_opn_settings_meksiabdou',
			'api_opn_admin_index_meksaibdou',
			array('label_for' => 'app_id_api_opn_meksiabdou')
		);
	}

	public function Key_api_opn_meksiabdou()
	{
		$input =  '<input type="text" 
					class="regular-text"
					name="Key_api_opn_meksiabdou" 
					value="'.esc_attr( get_option( 'Key_api_opn_meksiabdou' )).'" 
					placeholder="YOUR REST API KEY">';
		echo $input;			
	}

	public function app_id_api_opn_meksiabdou()
	{
		$input =  '<input type="text" 
					class="regular-text"
					name="app_id_api_opn_meksiabdou" 
					value="'.esc_attr( get_option( 'app_id_api_opn_meksiabdou' )).'" 
					placeholder="APP ID">';			
		echo $input;			
	}

	public function setSections()
	{
		add_settings_section(
			'api_opn_admin_index_meksaibdou',
			'Settings',
			function() {echo '';},
			'api_opn_settings_meksiabdou'
		);
	}

	public function registerFields()
	{
		register_setting( 
			'api_opn_settings_meksiabdou_options_group',
			'Key_api_opn_meksiabdou',
			function($input){ return $input;}
		);
		register_setting( 
			'api_opn_settings_meksiabdou_options_group',
			'app_id_api_opn_meksiabdou',
			function($input){ return $input;}
		);
		$this->setSections();
		$this->setFields();
	}
}